/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hawla.ipim.emergencycorridorserver.json;

import de.hawla.ipim.emergencycorridorserver.models.GPSLocation;

/**
 *
 * @author Julian
 */
public class Accident {
    
    private String id;
    private String street;
    private long timeInMilliSec;
    private GPSLocation accidentLocation;
    private GPSLocation trafficJamEndLocation;
    private String exitNearbyAccident;
    private String exitNearbyJamEnd;

    public Accident() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    } 
    
    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the timeInMilliSec
     */
    public long getTimeInMilliSec() {
        return timeInMilliSec;
    }

    /**
     * @param timeInMilliSec the timeInMilliSec to set
     */
    public void setTimeInMilliSec(long timeInMilliSec) {
        this.timeInMilliSec = timeInMilliSec;
    }

    /**
     * @return the accidentLocation
     */
    public GPSLocation getAccidentLocation() {
        return accidentLocation;
    }

    /**
     * @param accidentLocation the accidentLocation to set
     */
    public void setAccidentLocation(GPSLocation accidentLocation) {
        this.accidentLocation = accidentLocation;
    }

    /**
     * @return the trafficJamEndLocation
     */
    public GPSLocation getTrafficJamEndLocation() {
        return trafficJamEndLocation;
    }

    /**
     * @param trafficJamEndLocation the trafficJamEndLocation to set
     */
    public void setTrafficJamEndLocation(GPSLocation trafficJamEndLocation) {
        this.trafficJamEndLocation = trafficJamEndLocation;
    }

    /**
     * @return the exitNearbyAccident
     */
    public String getExitNearbyAccident() {
        return exitNearbyAccident;
    }

    /**
     * @param exitNearbyAccident the exitNearbyAccident to set
     */
    public void setExitNearbyAccident(String exitNearbyAccident) {
        this.exitNearbyAccident = exitNearbyAccident;
    }

    /**
     * @return the exitNearbyJamEnd
     */
    public String getExitNearbyJamEnd() {
        return exitNearbyJamEnd;
    }

    /**
     * @param exitNearbyJamEnd the exitNearbyJamEnd to set
     */
    public void setExitNearbyJamEnd(String exitNearbyJamEnd) {
        this.exitNearbyJamEnd = exitNearbyJamEnd;
    }

    @Override
    public String toString() {
        return "Accident{" + "id=" + id + ", street=" + street + ", timeInMilliSec=" + timeInMilliSec + ", accidentLocation=" + accidentLocation + ", trafficJamEndLocation=" + trafficJamEndLocation + ", exitNearbyAccident=" + exitNearbyAccident + ", exitNearbyJamEnd=" + exitNearbyJamEnd + '}';
    }
    
   
}
