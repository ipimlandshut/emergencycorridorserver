/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hawla.ipim.emergencycorridorserver.main;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.graphhopper.GraphHopper;
import com.graphhopper.matching.EdgeMatch;
import com.graphhopper.matching.LocationIndexMatch;
import com.graphhopper.routing.util.CarFlagEncoder;
import com.graphhopper.routing.util.EncodingManager;
import com.graphhopper.matching.MapMatching;
import com.graphhopper.matching.MatchResult;
import com.graphhopper.storage.GraphHopperStorage;
import com.graphhopper.storage.index.LocationIndexTree;
import com.graphhopper.util.EdgeIteratorState;
import com.graphhopper.util.GPXEntry;
import de.hawla.ipim.emergencycorridorserver.exception.MapMatchingException;
import de.hawla.ipim.emergencycorridorserver.models.GPSLocation;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;

/**
 *
 * @author Julian
 */
public class MapMatchingController {
    
    private static final Logger LOGGER = Logger.getLogger("MapMatchingController.java");
    
    private final static PropertyHandler PROPERTY_HANDLER = PropertyHandler.getInstance();
    private final static String DEFAULT_PATH = PROPERTY_HANDLER.getPathToMap();
    //for version below 0.7.0
    private GraphHopper hopper = new GraphHopper();
    //version for Graphhopper version 0.8.x
//    private GraphHopperOSM hopper = new GraphHopperOSM();
    private CarFlagEncoder encoder = new CarFlagEncoder();
    

    public MapMatchingController() {
        String osmFile = DEFAULT_PATH + PROPERTY_HANDLER.getOsmFileName();
     
        //version for Graphhopper version 0.8.x
//        hopper.setDataReaderFile(osmFile);
        //for version below 0.7.0
//        hopper.setOSMFile(osmFile);
        hopper.setOSMFile(osmFile);
        
        
        hopper.setGraphHopperLocation(PROPERTY_HANDLER.getGraphhopperWorkDirectory());
        hopper.setEncodingManager(new EncodingManager(encoder));
        hopper.importOrLoad();
    }
        
    
    public String getStreetName(List<GPXEntry> gpsCoordinates) throws MapMatchingException{
//        gpsCoordinates = optimizeGPSPoints(gpsCoordinates);
        //for Graphhopper version 0.8.x
//        String algorithm = Parameters.Algorithms.DIJKSTRA_BI;        
//        Weighting weighting = new FastestWeighting(encoder);
//        AlgorithmOptions algoOptions = new AlgorithmOptions(algorithm, weighting);
//        MapMatching mapMatching = new MapMatching(hopper, algoOptions);
//        mapMatching.setMeasurementErrorSigma(0.00000000000000000001d);
        // create MapMatching object, can and should be shared accross threads
//        AlgorithmOptions chOpts = AlgorithmOptions.start()
//                .maxVisitedNodes(1000)
//                .hints(new PMap().put(Parameters.CH.DISABLE, false))
//                .build();
//        AlgorithmOptions flexibleOptions = AlgorithmOptions.start().build();
//        MapMatching mapMatching = new MapMatching(hopper, flexibleOptions);

        // for Graphhopper version 0.7.0 below
        GraphHopperStorage graph = hopper.getGraphHopperStorage();
        LocationIndexMatch locationIndex = new LocationIndexMatch(graph,
                (LocationIndexTree) hopper.getLocationIndex());
        MapMatching mapMatching = new MapMatching(graph, locationIndex, encoder);
        
        MatchResult matchResult = mapMatching.doWork(gpsCoordinates);
        // return GraphHopper edges with all associated GPX entries
        List<EdgeMatch> matches = matchResult.getEdgeMatches();
        // now do something with the edges like storing the edgeIds or doing fetchWayGeometry etc
        System.out.println("number of matches: " + matches.size());
        
        Map<String, Long> results = new HashMap<>();
        for(EdgeMatch edge : matches){
            EdgeIteratorState edgeIteratorState = edge.getEdgeState();
            String edgeName = edgeIteratorState.getName();
            if(results.containsKey(edgeName)){
                results.put(edgeName, results.get(edgeName)+1L);
            } else if (!edgeName.isEmpty() || !("").equals(edgeName)){
                results.put(edgeName, 1L);
            }
            
            LOGGER.debug("Edge state: " + edge.getEdgeState() + "\nEdge toString: " + edge.toString());
            System.out.println("Edge state: " + edge.getEdgeState() + "\nEdge toString: " + edge.toString() + " isForward? "+ edgeIteratorState.isForward(encoder) 
                    + " isBackward? " + edgeIteratorState.isBackward(encoder));
            LOGGER.debug("edge name :"  + edge.getEdgeState().getName() + "; min distance: " + edge.getEdgeState().getDistance());
            System.out.println("edge name :"  + edge.getEdgeState().getName() + "; min distance: " + edge.getEdgeState().getDistance() );
            
        }
        if(!results.isEmpty()){
            String bestMatch = "";
            long highestCount = 0L;
            for(String key : results.keySet()){
                long currentCount = results.get(key);
                if(currentCount > highestCount){
                    highestCount = currentCount;
                    bestMatch = key;
                }if(currentCount == highestCount){
                    String pattern = "[AB]\\s\\d+";
                    if(key.matches(pattern)){
                        bestMatch = key;
                    }
                }
            }
            return bestMatch;
        }else{
            LOGGER.warn("Found no matching street for users GPS-Coordinates!");
            throw new MapMatchingException("Found no matching street for users GPS-Coordinates!");
        }        
    }

    public List<GPXEntry> getGPSPoints(String json) {
        Gson gson = new Gson();
        Type collectionType = new TypeToken<ArrayList<GPSLocation>>(){}.getType();
        List<GPSLocation> receivedGpsPoints = gson.fromJson(json, collectionType);
        List<GPXEntry> gpsPoints = new ArrayList<>();
        for(GPSLocation currentEntry: receivedGpsPoints){
            gpsPoints.add(new GPXEntry(currentEntry.getLatitude(), currentEntry.getLongitude(), 0, currentEntry.getTimeInMilliSec()));
        }
        
        return gpsPoints;
    }

    /**
     * This method checks that the gps points are not equals.
     * If so than a minor change will be procceded.
     * @param gpsCoordinates coordinates to check
     * @return optimized gpspositions
     */
    private List<GPXEntry> optimizeGPSPoints(List<GPXEntry> gpsCoordinates) {
        if(gpsCoordinates.size()==2){
            Set<GPXEntry> gpsSet = new HashSet<>();
            for(GPXEntry currentEntry : gpsCoordinates){
               gpsSet.add(currentEntry);
            }
            if(gpsSet.size() == 1){
                List<GPXEntry> newList = new ArrayList<>();
                newList.addAll(gpsSet);
                GPXEntry secondGPSPoint = new GPXEntry(newList.get(0).getLat()+0.00001, newList.get(0).getLon()-0.00001, 0);
                newList.add(secondGPSPoint);
                return newList;
            }
            return gpsCoordinates;
        }
        return gpsCoordinates;
    }
    
    
}
