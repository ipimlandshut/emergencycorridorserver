package de.hawla.ipim.emergencycorridorserver.xmlconvert;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.xml.stream.*;
import javax.xml.transform.stream.StreamSource;
import de.hawla.ipim.emergencycorridorserver.json.Accident;
import de.hawla.ipim.emergencycorridorserver.main.PropertyHandler;
import de.hawla.ipim.emergencycorridorserver.models.GPSLocation;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
/**
 *
 * @author Markus
 */
public class XMLConverter {
    
    private static final Logger LOGGER = Logger.getLogger(XMLConverter.class);
    
    public static Accident analyze(String filepath) throws FileNotFoundException, ParserConfigurationException, SAXException, IOException{
        File f = new File(filepath);
        if(f.exists()==false) {
            throw new FileNotFoundException("Not Found: "+filepath);
        }
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dbBuilder = dbFactory.newDocumentBuilder();
        Document doc = dbBuilder.parse(filepath);
        doc.getDocumentElement().normalize();
        
        NodeList codNodes = doc.getElementsByTagName("COD");
        boolean relevantAccidentFound = false;
        for(int i = 0; i < codNodes.getLength(); i++){
            Node currentEvtNode = codNodes.item(i);
            if("1974".equals(currentEvtNode.getTextContent()) && "1".equals(currentEvtNode.getNextSibling().getNextSibling().getTextContent())
                    || "122".equals(currentEvtNode.getTextContent()) && "2".equals(currentEvtNode.getNextSibling().getNextSibling().getTextContent())
                    || "123".equals(currentEvtNode.getTextContent()) && "2".equals(currentEvtNode.getNextSibling().getNextSibling().getTextContent())){
                
                relevantAccidentFound = true;
                break;
            }
        }
        if(relevantAccidentFound){    
            Accident accident = new Accident();
            return fillAccident(doc, accident);
        }
        else{
            return null;    
        }
        
    }
    
     private static Accident fillAccident(Document doc, Accident accident) {
        //get id
        NodeList tidList = doc.getElementsByTagName("TID");
        for(int i = 0; i<tidList.getLength(); i++){
            String accidentId = tidList.item(i).getTextContent();
            if(!accidentId.isEmpty()){
                accident.setId(accidentId);
            }
        }
        //get time
        NodeList nvrList = doc.getElementsByTagName("NVR");
        for(int i = 0; i<nvrList.getLength(); i++){
            String accidentTime = nvrList.item(i).getTextContent();
            if(!accidentTime.isEmpty()){
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                accidentTime = accidentTime.substring(0, 10)+" "+accidentTime.substring(11, 16);
                accident.setTimeInMilliSec(LocalDateTime.parse(accidentTime, formatter).atZone(ZoneId.of("Europe/Paris")).toInstant().toEpochMilli());
            }
            
        }
        //get traffic coordinates
        NodeList corList = doc.getElementsByTagName("COR");
        List<GPSLocation> trafficCoordinates = new ArrayList<>();
        for(int i = 0; i<corList.getLength(); i++){
            NodeList corChildList = corList.item(i).getChildNodes();
            GPSLocation gpsLocation = new GPSLocation();
            for(int j = 0; j < corChildList.getLength(); j++){
                Node currentChildNode = corChildList.item(j);
                if("LAT".equals(currentChildNode.getNodeName())){
                    gpsLocation.setLatitude(Double.parseDouble(currentChildNode.getTextContent()));
                } else if("LON".equals(currentChildNode.getNodeName())){
                    gpsLocation.setLongitude(Double.parseDouble(currentChildNode.getTextContent()));
                }
            }            
            if(gpsLocation.getLatitude() != 0 && gpsLocation.getLongitude() != 0){
                trafficCoordinates.add(gpsLocation);
            }
        }
        if(trafficCoordinates.size() >= 2){
            accident.setAccidentLocation(trafficCoordinates.get(trafficCoordinates.size()-1));
            accident.setTrafficJamEndLocation(trafficCoordinates.get(0));
        }
        
        //set exit nearby traffic jam end and street name
        NodeList staList = doc.getElementsByTagName("STA");
        
        for(int i = 0; i<staList.getLength(); i++){
            NodeList currentStaChildList = staList.item(i).getChildNodes();
            
            for(int j = 0; j<currentStaChildList.getLength(); j++){
                Node currentStaChild = currentStaChildList.item(j);
                if("DES".equals(currentStaChild.getNodeName())){
                    NodeList desChildList = currentStaChild.getChildNodes();
                    
                    for(int k = 0; k<desChildList.getLength(); k++){
                        Node currentDesNode = desChildList.item(k);
                        if("RNA".equals(currentDesNode.getNodeName())){
                            NodeList rnaChildList = currentDesNode.getChildNodes();
                         
                            for(int l = 0; l < rnaChildList.getLength(); l++){
                                Node currentRnaNode = rnaChildList.item(l);
                                if("NAM".equals(currentRnaNode.getNodeName())){
                                    NodeList namNodeList = currentRnaNode.getChildNodes();
                                   
                                    for(int m = 0; m<namNodeList.getLength(); m++){
                                        if("TEX".equals(namNodeList.item(m).getNodeName())){
                                            accident.setExitNearbyJamEnd(namNodeList.item(m).getTextContent());
                                        }
                                    }
                                } 
                            } 
                        } else if("RNU".equals(currentDesNode.getNodeName())){
                            NodeList rnuChildList = currentDesNode.getChildNodes();
                            for(int l = 0; l < rnuChildList.getLength(); l++){
                                Node currentChildNode = rnuChildList.item(l);
                                if("NUM".equals(currentChildNode.getNodeName())){
                                    String accidentStreet = currentChildNode.getTextContent();
                                    Pattern patter = Pattern.compile("\\D+");
                                    Matcher matcher = patter.matcher(accidentStreet);
                                    if(matcher.find()){
                                        int matcherEnd = matcher.end();
                                        accident.setStreet(accidentStreet.substring(matcher.start(), matcherEnd) + " " + accidentStreet.substring(matcherEnd, accidentStreet.length()));
                                    }  
                                } 
                            } 
                        }    
                    }
                }
            }
        }
        
        //get exit nearby accident
        NodeList endList = doc.getElementsByTagName("END");
        for(int h = 0; h<endList.getLength(); h++){
        NodeList currentEndChildList = endList.item(h).getChildNodes();
            for(int i = 0; i<currentEndChildList.getLength(); i++){
                Node currentEndCild = currentEndChildList.item(i);
                if("DES".equals(currentEndCild.getNodeName())){
                        NodeList desChildList = currentEndCild.getChildNodes();

                        for(int k = 0; k<desChildList.getLength(); k++){
                            Node currentDesNode = desChildList.item(k);
                            if("RNA".equals(currentDesNode.getNodeName())){
                                NodeList rnaChildList = currentDesNode.getChildNodes();

                                for(int l = 0; l < rnaChildList.getLength(); l++){
                                    Node currentRnaNode = rnaChildList.item(l);
                                    if("NAM".equals(currentRnaNode.getNodeName())){
                                        NodeList namNodeList = currentRnaNode.getChildNodes();

                                        for(int m = 0; m<namNodeList.getLength(); m++){
                                            if("TEX".equals(namNodeList.item(m).getNodeName())){
                                                accident.setExitNearbyAccident(namNodeList.item(m).getTextContent());
                                            }
                                        }
                                    } 
                                } 
                            }
                        }
                }
            }
        }
        
        return accident;
    }
     
     public static List<Accident> analyzeDir () throws FileNotFoundException, ParserConfigurationException, SAXException, IOException  {

        PropertyHandler propertyHandler = PropertyHandler.getInstance();
        String filepath = propertyHandler.getPathToXmlFiles();

        List<Accident> accs = new ArrayList<Accident>();

        Accident a = null;
        File f = new File(filepath);
        if(f.exists()==false)   { throw new FileNotFoundException("Not Found: "+filepath);}
        File[] files = f.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".xml");
            }
        });

        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                a = XMLConverter.analyze(files[i].getAbsolutePath());
                if(a!=null) accs.add(a);
            }
        }
        return accs;
    }   
    
    public static Accident convert (String filepath) throws FileNotFoundException, XMLStreamException {
        File f = new File(filepath);
        ArrayList<GPSLocation> coords = new ArrayList<GPSLocation>();
        if(f.exists()==false) {
            throw new FileNotFoundException("Not Found: "+filepath);
        }
        
        
        XMLInputFactory xif = XMLInputFactory.newFactory();
        StreamSource xml = new StreamSource(filepath);
        XMLStreamReader xsr = xif.createXMLStreamReader(xml);
        
        boolean laneEventFound = false;
        Accident eme = new Accident();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        
        String cod = "";
        String type = "";
        
        //Schleife um XML zu durchsuchen
        while(xsr.hasNext()) {
            
            
            //TMC Code für ERLane suchen. Achtung Typ muss passen
            if(xsr.isStartElement() && xsr.getLocalName().equals("EVT")) {
                while (xsr.hasNext()){
                    
                                     
                    if(xsr.isStartElement() && xsr.getLocalName().equals("TMC")) {
                        
                        cod = type = "";
                        
                        while (xsr.hasNext()){
                            
                            if(xsr.isEndElement() && xsr.getLocalName().equals("TMC")) break;
                                                        
                            while (xsr.hasNext()){
                                if(xsr.isStartElement() && xsr.getLocalName().equals("COD")) {
                                    if (xsr.hasNext())xsr.next();
                                    cod = xsr.getText();
                                    if (        ("123".equals(cod) && "2".equals(type))
                                             || ("122".equals(cod) && "2".equals(type))
                                             || ("1974".equals(cod) && "1".equals(type))
                                        ){
                                          //Gefunden ... 
                                          laneEventFound = true; 
                                         } 
                                    break;
                                }
                                if (xsr.hasNext())xsr.next();
                            }
                            while (xsr.hasNext()){
                                if(xsr.isStartElement() && xsr.getLocalName().equals("TYP")) {
                                    if (xsr.hasNext())xsr.next();
                                    type = xsr.getText();
                                    if (        ("123".equals(cod) && "2".equals(type))
                                             || ("122".equals(cod) && "2".equals(type))
                                             || ("1974".equals(cod) && "1".equals(type))
                                        ){
                                          //Gefunden ... 
                                          laneEventFound = true; 
                                         } 
                                    break;
                                }
                                if (xsr.hasNext())xsr.next();
                           }
                           if (xsr.hasNext())xsr.next();
                        }
                        break;
                    }
                    
                  if (xsr.hasNext())xsr.next();
                }               
            }
            
            //TID Id für Stau suchen
            if(xsr.isStartElement() && xsr.getLocalName().equals("TID")) {
                if (xsr.hasNext())xsr.next();
                eme.setId(xsr.getText());
                
            }

            
            //Uhrzeit Stau zu suchen
            if(xsr.isStartElement() && xsr.getLocalName().equals("NVR")) {
                if (xsr.hasNext())xsr.next();
                String timetext = xsr.getText();
                timetext = timetext.substring(0, 10)+" "+timetext.substring(11, 16);
                eme.setTimeInMilliSec(LocalDateTime.parse(timetext, formatter).atZone(ZoneId.of("Europe/Paris")).toInstant().toEpochMilli()); 
                    
             }
            

            
            //Coords Stau sammeln
            if(xsr.isStartElement() && xsr.getLocalName().equals("COR")) {
                GPSLocation g = new GPSLocation();
                while (xsr.hasNext()){
                    if(xsr.isStartElement() && xsr.getLocalName().equals("LAT")) {
                        if (xsr.hasNext())xsr.next();
                        g.setLatitude(Double.parseDouble(xsr.getText()));
                        break;
                    }
                    if (xsr.hasNext())xsr.next();
                }
                while (xsr.hasNext()){
                    if(xsr.isStartElement() && xsr.getLocalName().equals("LON")) {
                        if (xsr.hasNext())xsr.next();
                        g.setLongitude(Double.parseDouble(xsr.getText()));
                        coords.add(g);
                        break;
                    }
                    if (xsr.hasNext())xsr.next();
                }
                
             }
            
            //Find Road Number
            if(xsr.isStartElement() && xsr.getLocalName().equals("END")) {
                while (xsr.hasNext()){
                    if(xsr.isStartElement() && xsr.getLocalName().equals("RNU")) {
                        while (xsr.hasNext()){
                           if(xsr.isStartElement() && xsr.getLocalName().equals("NUM")) {
                              if (xsr.hasNext())xsr.next();
                                String accidentStreet = xsr.getText();
                                Pattern patter = Pattern.compile("\\D+");
                                Matcher matcher = patter.matcher(accidentStreet);
                                if(matcher.find()){
                                    int matcherEnd = matcher.end();
                                    eme.setStreet(accidentStreet.substring(matcher.start(), matcherEnd) + " " + accidentStreet.substring(matcherEnd, accidentStreet.length()));
                                } else {
                                    eme.setStreet(accidentStreet);
                                }
                              break;
                           }
                           if (xsr.hasNext())xsr.next();
                        }
                        break;
                    }
                    
                  if (xsr.hasNext())xsr.next();
                }               
            }
            
            if(xsr.isStartElement() && xsr.getLocalName().equals("JBH")) {
                while (xsr.hasNext()){
                    if(xsr.isStartElement() && xsr.getLocalName().equals("RNA")) {
                        while (xsr.hasNext()){
                           if(xsr.isStartElement() && xsr.getLocalName().equals("TEX")) {
                              if (xsr.hasNext())xsr.next();
                              eme.setExitNearbyJamEnd(xsr.getText());
                              break;
                           }
                           if (xsr.hasNext())xsr.next();
                        }     
                        break;
                    }
                  if (xsr.hasNext())xsr.next();
                }                
            }
            
            if(xsr.isStartElement() && xsr.getLocalName().equals("JAH")) {
                while (xsr.hasNext()){
                    if(xsr.isStartElement() && xsr.getLocalName().equals("RNA")) {
                        while (xsr.hasNext()){
                           if(xsr.isStartElement() && xsr.getLocalName().equals("TEX")) {
                              if (xsr.hasNext())xsr.next();
                              eme.setExitNearbyAccident(xsr.getText());
                              break;
                           }
                           if (xsr.hasNext())xsr.next();
                        }     
                        break;
                    }
                  if (xsr.hasNext())xsr.next();
                }                
            }
            
            if (xsr.hasNext())xsr.next();
        }
        
        xsr.close();
        
        if (coords.size()>=2) {
            eme.setAccidentLocation(coords.get(coords.size()-1));
            eme.setTrafficJamEndLocation(coords.get(0));
        }
        
        if (laneEventFound) return eme; 
        return null;
    }
    
    
    
    public static List<Accident> convertDir () throws FileNotFoundException, XMLStreamException  {

        PropertyHandler propertyHandler = PropertyHandler.getInstance();
        String filepath = propertyHandler.getPathToXmlFiles();

        List<Accident> accs = new ArrayList<Accident>();

        Accident a = null;
        File f = new File(filepath);
        if(f.exists()==false)   { throw new FileNotFoundException("Not Found: "+filepath);}
        File[] files = f.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".xml");
            }
        });

        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                a = XMLConverter.convert(files[i].getAbsolutePath());
                if(a!=null) accs.add(a);
            }
        }
        return accs;
    }   
    
    public static void toArchive(File afile)throws FileNotFoundException{

        PropertyHandler propertyHandler = PropertyHandler.getInstance();
        String filepath = propertyHandler.getPathToXmlFiles()+ File.separator + "archive" + File.separator;

        try {

            if(!new File(filepath).exists()) new File(filepath).mkdir();

               if(afile.renameTo(new File(filepath + File.separator + afile.getName()))){

               }else{
                    throw new Exception("Could not move File: "+afile.getAbsolutePath() +" to "+filepath);
               }
            }catch(Exception e){
                    throw new FileNotFoundException("Not Found: "+afile.getAbsolutePath());
            }

    }    

    public static void archiveAll()throws FileNotFoundException{

        PropertyHandler propertyHandler = PropertyHandler.getInstance();
        String filepath = propertyHandler.getPathToXmlFiles();


        File f = new File(filepath);
        if(f.exists()==false)   { throw new FileNotFoundException("Not Found: "+filepath);}
        File[] files = f.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".xml");
            }
        });

        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                try {
                    XMLConverter.toArchive(files[i]);
                }catch(Exception e){
                    throw new FileNotFoundException("Could not Archive: "+files[i].getAbsolutePath());
                }
            }
            LOGGER.debug("Files moved successfully!");
        }
    }

    public static void returnAllFromArchive()throws FileNotFoundException{

        PropertyHandler propertyHandler = PropertyHandler.getInstance();
        String filepath = propertyHandler.getPathToXmlFiles() + File.separator + "archive" + File.separator;
        String testdir  = propertyHandler.getPathToXmlFiles() + File.separator;  



        File f = new File(filepath);
        if(f.exists()==false)   { throw new FileNotFoundException("Not Found: "+filepath);}
        File[] files = f.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".xml");
            }
        });

        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                try {
                    if(files[i].renameTo(new File(testdir + files[i].getName()))){
                    }else{
                        throw new Exception("Could not move File: "+files[i].getAbsolutePath() +" to "+testdir);
                    }
                }catch(Exception e){
                        throw new FileNotFoundException("Not Found: "+files[i].getAbsolutePath());
                }
            }
        }
        LOGGER.debug("All files successfully moved");

    }

   


    
}
