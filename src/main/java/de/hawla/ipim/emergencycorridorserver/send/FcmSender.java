/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hawla.ipim.emergencycorridorserver.send;

import de.hawla.ipim.emergencycorridorserver.main.PropertyHandler;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;



/**
 *
 * @author Julian
 */
public class FcmSender {
    
    final static Logger LOGGER = Logger.getLogger(FcmSender.class);
    
    /**
     * Sends the jam message with the given json. 
     * 
     * @param accidentJsonData the json with the accident data
     * @return status code from firebase
     * @throws UnsupportedEncodingException thrown if the json contains unsupported chars 
     * @throws IOException 
     */
    public int sendJamMessage(String accidentJsonData) throws UnsupportedEncodingException, IOException{
        PropertyHandler propertyHandler = PropertyHandler.getInstance();
        String serverKey = propertyHandler.getGooglerFirebaseServerKey();
        String serverUrl = propertyHandler.getGoogleFirebaseUrl();
        HttpClient httpClient = HttpClientBuilder.create().build();

        HttpPost request = new HttpPost(serverUrl);
        String json = "{  \"data\" :" + accidentJsonData +
                ",\"to\" : \"/topics/" + propertyHandler.getGoogleTopicGroup()+ "\"," +
                "\"priority\": \"high\"," +
                "\"content_available\"  : true, \n"+ 
                "\"time_to_live\" : "  + propertyHandler.getExpirationTimeForFcmMessagesInSec() + "}"; 
        LOGGER.debug("compelte json for google fcm:\n" + json);
        StringEntity params =new StringEntity(json, "UTF-8");
        request.addHeader("Content-Type", "application/json; charset=UTF-8");
        request.addHeader("Authorization", "key=" + serverKey);
        request.setEntity(params);
        HttpResponse response = httpClient.execute(request);

   
        int statuscode = response.getStatusLine().getStatusCode();
        LOGGER.info("Response code from firebase: " + statuscode);

        if(statuscode<200 || statuscode>300){
            LOGGER.error("Firebase is not accepting the json. HTTP status code: " + statuscode);
            LOGGER.error("response from firebase server: " + response.toString());
        }
        return statuscode;

        
    }
    
}
