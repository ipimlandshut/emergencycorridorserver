/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hawla.ipim.emergencycorridorserver.main;

import de.hawla.ipim.emergencycorridorserver.send.FcmSender;
import java.io.IOException;

/**
 *
 * @author Julian
 */
public class FCMController {
    
    FcmSender fcmSender;

    public FCMController() {
        super();
        fcmSender = new FcmSender();
    }
    
    public int sendAccidentDataToPhones(String accidentJsonData) throws IOException{
        return fcmSender.sendJamMessage(accidentJsonData);
    }
    
}
