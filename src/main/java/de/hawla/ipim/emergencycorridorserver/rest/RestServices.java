/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hawla.ipim.emergencycorridorserver.rest;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.graphhopper.util.GPXEntry;
import de.hawla.ipim.emergencycorridorserver.exception.MapMatchingException;
import de.hawla.ipim.emergencycorridorserver.main.FCMController;
import de.hawla.ipim.emergencycorridorserver.main.MapMatchingController;
import de.hawla.ipim.emergencycorridorserver.main.XmlController;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.stream.XMLStreamException;
import org.apache.log4j.Logger;

/**
 *
 * @author Julian
 */
@Path("/services")
public class RestServices {
    
  final static Logger LOGGER = Logger.getLogger(RestServices.class);
  private final MapMatchingController mapMatchingController = new MapMatchingController();
  private final FCMController fcmController = new FCMController();
    

  // This method is called if HTML is request
  @GET
  @Produces(MediaType.TEXT_HTML)
  public String sayHtmlHello() {
    return "<html> " + "<title>" + "Hello" + "</title>"
        + "<body><h1>" + "This is the rest service for the emergency corridor notification." + "</body></h1>" + "</html> ";
  }
    
    @POST
    @Path("/getStreet")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStreet(String json) {
        try {
            LOGGER.debug("Json recieved : " + json);
            System.out.print("Json recieved : " + json);
            List<GPXEntry> gpsList = mapMatchingController.getGPSPoints(json);
            String streetName = mapMatchingController.getStreetName(gpsList);

            LOGGER.debug("Matched street name: " + streetName);

            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("streetName", streetName);
            return Response.ok(jsonResponse.toString(), MediaType.APPLICATION_JSON).build();
        } catch (MapMatchingException ex) {
            LOGGER.error("Can't map gps point to street because of an internal error.", ex);
            return Response.ok("", MediaType.APPLICATION_JSON).build();
        }
    }
    
    @POST
    @Path("/sendTestJamMessage")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response sendTestJamMessage(String json) {

        LOGGER.debug("Json recieved : " + json);
//        System.out.print("Json recieved : " + json);
        if(json != null && !json.isEmpty()){
            try {
                fcmController.sendAccidentDataToPhones(json);
                return Response.ok(MediaType.APPLICATION_JSON).build();
            } catch (IOException ex) {
                LOGGER.error("Could send xml files.", ex);
                return Response.serverError().entity("Couldn't send xml files because of an internal server error.\nError: " + ex.getMessage()).build();
            }
        } else {
            LOGGER.info("Could send xml files. Json was empty or null");
            return Response.serverError().entity("Could send xml files. Json was empty or null").build();
            
        }
        

 
    }
    
    @POST
    @Path("/readXMLFiles")
    public Response readXMLFiles(){
        LOGGER.debug("start reading the xml files");
        XmlController xmlController = new XmlController();
        int statusCodeFirebase;
      try {
          String json = xmlController.getAccidentsAsJson().trim();
          if(json != null && !json.isEmpty() && !"".equals(json)){  
            statusCodeFirebase = fcmController.sendAccidentDataToPhones(json);
            return Response.ok("Response code friebase: " + statusCodeFirebase, MediaType.APPLICATION_JSON).build();
          }
          return Response.ok(MediaType.APPLICATION_JSON).build();
      } catch (FileNotFoundException | XMLStreamException ex) {
          LOGGER.error("Could read xml files.", ex);
          return Response.serverError().entity("Couldn't read xml files because of an internal server error.\nError: " + ex.getMessage()).build();
      } catch (IOException ex) {
          LOGGER.error("Could send xml files.", ex);
          return Response.serverError().entity("Couldn't send xml files because of an internal server error.\nError: " + ex.getMessage()).build();
      }
    }
    
    @POST
    @Path("/moveXMLFilesToArchive")
    public Response moveXMLFilesToArchive(){
        LOGGER.debug("start moving the xml files");
        XmlController xmlController = new XmlController();
      try {
          xmlController.moveXMLFilesToArchive();
      } catch (FileNotFoundException ex) {
          LOGGER.error("Could read xml files.", ex);
          return Response.serverError().entity("Couldn't move xml files because of an internal server error.\nError: " + ex.getMessage()).build();
      }
        return Response.ok(MediaType.APPLICATION_JSON).build();
    }
    
    
}
