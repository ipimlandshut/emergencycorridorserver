/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hawla.ipim.emergencycorridorserver.main;

import com.google.gson.Gson;
import de.hawla.ipim.emergencycorridorserver.json.Accident;
import de.hawla.ipim.emergencycorridorserver.xmlconvert.XMLConverter;
import java.io.FileNotFoundException;
import java.util.List;
import javax.xml.stream.XMLStreamException;
import org.apache.log4j.Logger;

/**
 *
 * @author Julian
 */
public class XmlController {

    private static final Logger LOGGER = Logger.getLogger(XmlController.class);
    public XmlController() {
    }
    
    public String getAccidentsAsJson() throws FileNotFoundException, XMLStreamException{
        String jsonToReturn;
        
        List<Accident> accidentList = XMLConverter.convertDir();
        Gson gson = new Gson();
        jsonToReturn = gson.toJson(accidentList);
        if(accidentList.isEmpty()){
            LOGGER.debug("No emergency corridor accidents found!");
            return "";
        }
        else if(accidentList.size()==1){
            jsonToReturn = "{\"accidentList\":" + jsonToReturn.subSequence(1, jsonToReturn.length()-1).toString() + "}";
        } else {
            jsonToReturn = "{\"accidentList\":" + jsonToReturn + "}";
        }
                
        return jsonToReturn;
    }
    
    public void moveXMLFilesToArchive() throws FileNotFoundException{
        XMLConverter.archiveAll();
    }
    
    

    
}
