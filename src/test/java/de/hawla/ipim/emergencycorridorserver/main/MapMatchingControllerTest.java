/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hawla.ipim.emergencycorridorserver.main;

import de.hawla.ipim.emergencycorridorserver.main.MapMatchingController;
import com.google.gson.Gson;
import com.graphhopper.util.GPXEntry;
import de.hawla.ipim.emergencycorridorserver.exception.MapMatchingException;
import de.hawla.ipim.emergencycorridorserver.models.GPSLocation;
import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Julian
 */
public class MapMatchingControllerTest {
    
    private static Logger LOGGER = Logger.getLogger(MapMatchingControllerTest.class);
    private MapMatchingController mapMatchingController;
    
    public MapMatchingControllerTest() {
        mapMatchingController = new MapMatchingController();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of sendActualJsonFile method, of class Controller.
     */
    @Test
    public void testGetStreetNameWithWiderGPSPoints() throws MapMatchingException {
        
         List<GPXEntry> gpsCoordinates = new ArrayList<GPXEntry>();
        
        //example with closer distances between the single gps points on A95 
        gpsCoordinates.add(new GPXEntry(47.89654, 11.39691, 0, System.currentTimeMillis()));
        gpsCoordinates.add(new GPXEntry(47.89650, 11.39690, 0, System.currentTimeMillis()));

        
        LOGGER.info("testGetStreetName");
        String streetName = mapMatchingController.getStreetName(gpsCoordinates);
        LOGGER.info("Street name is: " + streetName);
        System.out.println("Street name is: " + streetName);
        Assert.assertNotNull(streetName);
        Assert.assertEquals("A 95", streetName);
        
    }
    
    /**
     * Test of sendActualJsonFile method, of class Controller.
     */
    @Test
    public void testGetStreetNameWithNarrowGPSPoints() throws MapMatchingException {
        
         List<GPXEntry> gpsCoordinates = new ArrayList<GPXEntry>();
         
        //example with bigger distances between the single gps points on A95
        gpsCoordinates.add(new GPXEntry(47.91927, 11.39700, 0, System.currentTimeMillis()));
        gpsCoordinates.add(new GPXEntry(47.91525, 11.39466, 0, System.currentTimeMillis()));
        gpsCoordinates.add(new GPXEntry(47.90824, 11.39506, 0, System.currentTimeMillis()));
        gpsCoordinates.add(new GPXEntry(47.89745, 11.39678, 0, System.currentTimeMillis()));
        
        LOGGER.info("testGetStreetName");
        String streetName = mapMatchingController.getStreetName(gpsCoordinates);
        LOGGER.info("Street name is: " + streetName);
        System.out.println("Street name is: " + streetName);
        Assert.assertNotNull(streetName);
        Assert.assertEquals("A 95", streetName);
        
    }
    
    
    /**
     * Test of sendActualJsonFile method, of class Controller.
     */
    @Test
    public void testGetStreetNameWithPointOnBridgeGPSPoints() throws MapMatchingException {
        
         List<GPXEntry> gpsCoordinates = new ArrayList<GPXEntry>();
         
        //example with bigger distances between the single gps points on A95
        gpsCoordinates.add(new GPXEntry(48.01438,11.41750, 0, System.currentTimeMillis()));
        gpsCoordinates.add(new GPXEntry(48.01494, 11.41752, 0, System.currentTimeMillis()));
        gpsCoordinates.add(new GPXEntry(48.01541, 11.41754, 0, System.currentTimeMillis()));
        gpsCoordinates.add(new GPXEntry(48.01557, 11.41756, 0, System.currentTimeMillis()));
        
        LOGGER.info("testGetStreetName");
        String streetName = mapMatchingController.getStreetName(gpsCoordinates);
        LOGGER.info("Street name is: " + streetName);
        System.out.println("Street name is: " + streetName);
        Assert.assertNotNull(streetName);
        Assert.assertEquals("A 95", streetName);
        
    }
    
    
    
    @Test
    public void testGetGPSPoints() {
        
         List<GPSLocation> originalGPSCoordinates = new ArrayList<GPSLocation>();
         
        //example with bigger distances between the single gps points on A95
        originalGPSCoordinates.add(new GPSLocation(48.01438,11.41750, System.currentTimeMillis()));
        originalGPSCoordinates.add(new GPSLocation(48.01494, 11.41752, System.currentTimeMillis()));
        originalGPSCoordinates.add(new GPSLocation(48.01541, 11.41754, System.currentTimeMillis()));
        originalGPSCoordinates.add(new GPSLocation(48.01557, 11.41756, System.currentTimeMillis()));
        
        Gson gson = new Gson();
        String jsonToTest = gson.toJson(originalGPSCoordinates);
        
        LOGGER.info("testGetGPSPoints");
        List<GPXEntry> returnedGPSCoordinates = mapMatchingController.getGPSPoints(jsonToTest);
        for(int i = 0; i < returnedGPSCoordinates.size()-1; i++){
            GPSLocation expectedLcoation = originalGPSCoordinates.get(i);
            Assert.assertEquals(new GPXEntry(expectedLcoation.getLatitude(), expectedLcoation.getLongitude(), 0, expectedLcoation.getTimeInMilliSec())
                    , returnedGPSCoordinates.get(i));
        }
    }
    
}
