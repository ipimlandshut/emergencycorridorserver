/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hawla.ipim.emergencycorridorserver.main;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author Julian
 */
public class PropertyHandler {

   
    
    private final Logger LOGGER = Logger.getLogger(PropertyHandler.class);
    
    private static PropertyHandler propertieHandler;
    
    private String pathToResources;
    private String googlerFirebaseServerKey;
    private String googleFirebaseUrl;
    private String googleTopicGroup;
    private String graphhopperWorkDirectory;
    private String osmFileName;
    private String pathToMap;
    private int expirationTimeForFcmMessagesInSec;
    private String pathToXmlFiles;

    public String getPathToXmlFiles() {
        return pathToXmlFiles;
    }

    public void setPathToXmlFiles(String pathToXmlFiles) {
        this.pathToXmlFiles = pathToXmlFiles;
    }
    
    public String getOsmFileName() {
        return osmFileName;
    }

    public void setOsmFileName(String osmFileName) {
        this.osmFileName = osmFileName;
    }

    public static PropertyHandler getPropertieHandler() {
        return propertieHandler;
    }

    public static void setPropertieHandler(PropertyHandler propertieHandler) {
        PropertyHandler.propertieHandler = propertieHandler;
    }

    public String getGraphhopperWorkDirectory() {
        return graphhopperWorkDirectory;
    }

    public void setGraphhopperWorkDirectory(String graphhopperWorkDirectory) {
        this.graphhopperWorkDirectory = graphhopperWorkDirectory;
    }
    
    private PropertyHandler(){
        super();
        loadParams(); 
    }
    
    public static synchronized PropertyHandler getInstance(){
        if(PropertyHandler.propertieHandler == null){
            PropertyHandler.propertieHandler = new PropertyHandler();
        }
        return propertieHandler;
    }
    
     public final void loadParams() {
        Properties props = new Properties();
        InputStream is = null;

        try {
            is = Thread.currentThread().getContextClassLoader().getResourceAsStream("EmergencyCorridorServer.properties");

            // Try loading properties from the file (if found)
            props.load( is );
        }
        catch ( IOException e ) {
            LOGGER.error("Cannot read EmergencyCorridorServer properties.", e);
        }
        setPathToResources(props.getProperty("pathToResources"));
        setGooglerFirebaseServerKey(props.getProperty("GoogleFirebaseKey"));
        setGoogleFirebaseUrl(props.getProperty("GoogleFirebaseUrl"));
        setGoogleTopicGroup(props.getProperty("GoogleTopicGroup"));
        setGraphhopperWorkDirectory(props.getProperty("GraphhopperWorkDirectory"));
        setOsmFileName(props.getProperty("OSMFileName"));
        setPathToMap(props.getProperty("pathToMap"));
        setExpirationTimeForFcmMessagesInSec(Integer.parseInt(props.getProperty("ExpirationTimeForFcmMessagesInSec")));
        setPathToXmlFiles(props.getProperty("pathToXmlFiles"));
        
        LOGGER.debug("Properties has been successfully read.");
    }

    /**
     * @return the pathToResources
     */
    public String getPathToResources() {
        return pathToResources;
    }

    /**
     * @param pathToResources the pathToResources to set
     */
    public void setPathToResources(String pathToResources) {
        this.pathToResources = pathToResources;
    }

    /**
     * @return the serverKey
     */
    public String getGooglerFirebaseServerKey() {
        return googlerFirebaseServerKey;
    }

    /**
     * @param serverKey the serverKey to set
     */
    public void setGooglerFirebaseServerKey(String serverKey) {
        this.googlerFirebaseServerKey = serverKey;
    }

    /**
     * @return the googleFirebaseUrl
     */
    public String getGoogleFirebaseUrl() {
        return googleFirebaseUrl;
    }

    /**
     * @param googleFirebaseUrl the googleFirebaseUrl to set
     */
    public void setGoogleFirebaseUrl(String googleFirebaseUrl) {
        this.googleFirebaseUrl = googleFirebaseUrl;
    }

    /**
     * @return the googleTopicGroup
     */
    public String getGoogleTopicGroup() {
        return googleTopicGroup;
    }

    /**
     * @param googleTopicGroup the googleTopicGroup to set
     */
    public void setGoogleTopicGroup(String googleTopicGroup) {
        this.googleTopicGroup = googleTopicGroup;
    }
    
     /**
     * @return the pathToMap
     */
    public String getPathToMap() {
        return pathToMap;
    }

    /**
     * @param pathToMap the pathToMap to set
     */
    public void setPathToMap(String pathToMap) {
        this.pathToMap = pathToMap;
    }

    public int getExpirationTimeForFcmMessagesInSec() {
        return expirationTimeForFcmMessagesInSec;
    }

    public void setExpirationTimeForFcmMessagesInSec(int expirationTimeForFcmMessagesInSec) {
        this.expirationTimeForFcmMessagesInSec = expirationTimeForFcmMessagesInSec;
    }
    
    
    
}
