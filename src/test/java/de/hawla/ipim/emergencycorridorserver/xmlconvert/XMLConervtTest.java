/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hawla.ipim.emergencycorridorserver.xmlconvert;

import org.junit.Assert;
import org.junit.Test;
import org.junit.*;
import de.hawla.ipim.emergencycorridorserver.json.*;
import de.hawla.ipim.emergencycorridorserver.models.GPSLocation;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 *
 * @author Markus
 */
public class XMLConervtTest {
    
    @Test
    public void basicTest() throws Exception{
        
        long timeBeforeTest = System.currentTimeMillis();
        Accident accident = XMLConverter.convert("src\\test\\resources\\XMLs\\2017_09_01_10_28_51.xml");
        long timeAfterTest = System.currentTimeMillis();
        long timeNeededForTest = timeAfterTest-timeBeforeTest;
        System.out.println("time needed for the test in milli sec: " + timeNeededForTest+ ".");
        
        Assert.assertNotNull(accident);
        Assert.assertEquals("LMS-BY/r_LMS-BY/1747_D  BY LMS-BY", accident.getId());
        Assert.assertEquals("Dreieck Starnberg", accident.getExitNearbyJamEnd());
        Assert.assertEquals("Wolfratshausen", accident.getExitNearbyAccident());
        Assert.assertEquals("A 95", accident.getStreet());
        Assert.assertEquals(new GPSLocation(47.90351, 11.39674), accident.getAccidentLocation());
        Assert.assertEquals(new GPSLocation(48.02076, 11.41912), accident.getTrafficJamEndLocation());

    }
    
    @Test
    public void basicTestDir() throws Exception{
        
        List<Accident> eme = new ArrayList<>();
        
        eme = XMLConverter.convertDir();
        
                
        Assert.assertTrue(eme.size()>=3);
        Assert.assertEquals("A 4", eme.get(0).getStreet());
        Assert.assertEquals("A 4", eme.get(1).getStreet());
        Assert.assertEquals("A 95", eme.get(2).getStreet());
        

    }
        
    @Test (expected = FileNotFoundException.class)
    public void filenotfoundTest() throws Exception{
        
        Accident eme = null;
        
        eme = XMLConverter.convert("src\\test\\jd\\XMLs\\2017_03_06_10_26_27.xml");

    }
    
    @Test 
    public void archiveXML() throws Exception{
        
        XMLConverter.toArchive(new File("src\\test\\resources\\XMLs\\2017_03_06_10_26_27.xml"));
        
        Assert.assertEquals(true, new File("src\\test\\resources\\XMLs\\archive\\2017_03_06_10_26_27.xml").exists());
        Assert.assertEquals(false, new File("src\\test\\resources\\XMLs\\2017_03_06_10_26_27.xml").exists());
                
    }
    
    @Test 
    public void archiveDirXML() throws Exception{
        
        XMLConverter.archiveAll();
        
        Assert.assertEquals(true, new File("src\\test\\resources\\XMLs\\archive\\2017_03_06_10_26_27.xml").exists());
        Assert.assertEquals(false, new File("src\\test\\resources\\XMLs\\2017_03_06_10_26_27.xml").exists());
    }
    
    @Test 
    public void analyzeTest() throws Exception{
        
         long timeBeforeTest = System.currentTimeMillis();
        Accident accident = XMLConverter.analyze("src\\test\\resources\\XMLs\\2017_09_01_10_28_51.xml");
        long timeAfterTest = System.currentTimeMillis();
        long timeNeededForTest = timeAfterTest-timeBeforeTest;
        System.out.print("time needed for the test in milli sec: " + timeNeededForTest);
        
        Assert.assertNotNull(accident);
        Assert.assertEquals("LMS-BY/r_LMS-BY/1747_D  BY LMS-BY", accident.getId());
        Assert.assertEquals("Dreieck Starnberg", accident.getExitNearbyJamEnd());
        Assert.assertEquals("Wolfratshausen", accident.getExitNearbyAccident());
        Assert.assertEquals("A 95", accident.getStreet());
        Assert.assertEquals(new GPSLocation(47.90351, 11.39674), accident.getAccidentLocation());
        Assert.assertEquals(new GPSLocation(48.02076, 11.41912), accident.getTrafficJamEndLocation());
    }
    
   @Test
    public void analyzeDirTest() throws Exception{
        
        List<Accident> eme = new ArrayList<>();
        
        eme = XMLConverter.analyzeDir();
                
        Assert.assertTrue(eme.size()>=3);
        Assert.assertEquals("A 4", eme.get(0).getStreet());
        

    }
    
    @After
    public void restoreFromArchiveXML() throws Exception{
        
        XMLConverter.returnAllFromArchive();
        
        Assert.assertEquals(false, new File("src\\test\\resources\\XMLs\\archive\\2017_03_06_10_26_27.xml").exists());
        Assert.assertEquals(true, new File("src\\test\\resources\\XMLs\\2017_03_06_10_26_27.xml").exists());
    }
    
    
    
}

