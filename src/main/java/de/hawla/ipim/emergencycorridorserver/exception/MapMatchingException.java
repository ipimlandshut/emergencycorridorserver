/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hawla.ipim.emergencycorridorserver.exception;

/**
 *
 * @author Julian
 */
public class MapMatchingException extends Exception{
    
    public MapMatchingException (String msg){
        super(msg);
    }
    
    public MapMatchingException (String msg, Throwable throwable){
        super(msg, throwable);
    }
    
    
}
